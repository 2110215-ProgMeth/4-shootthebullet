package ui;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import lib.ConfigurableOption;
import main.Main;

public class ConfigScreen extends BorderPane {
	private BorderPane optionPane;
	private Button newGame,viewScore;
	public ConfigScreen(){
		setPrefSize(ConfigurableOption.screenWidth, ConfigurableOption.screenHeight);
		
		Label title=new Label("Shoot the bullet");
		title.setFont(Font.font("Tahoma",FontWeight.BOLD,FontPosture.ITALIC,30));
		VBox top=new VBox();
		top.getChildren().add(title);
		top.setAlignment(Pos.CENTER);
		top.setBackground(new Background(new BackgroundFill(Paint.valueOf("blue"), null, null)));
		setTop(top);
		
		newGame=new Button("New Game");
		viewScore=new Button("High Score");
		FlowPane bottom=new FlowPane();
		bottom.setHgap(ConfigurableOption.screenWidth/8.0);
		bottom.setPadding(new Insets(5));
		bottom.setBackground(new Background(new BackgroundFill(Paint.valueOf("green"), null, null)));
		bottom.setAlignment(Pos.CENTER);
		bottom.getChildren().add(newGame);
		bottom.getChildren().add(viewScore);
		setBottom(bottom);
		
		
		optionPane = new BorderPane();
		FlowPane resolutionSetting=new FlowPane();
		resolutionSetting.setHgap(10);
		resolutionSetting.setVgap(5);
		resolutionSetting.setPadding(new Insets(10));
		resolutionSetting.setAlignment(Pos.CENTER);
		Label widthL = new Label("WIDTH");
		TextField widthF = new TextField(String.valueOf(ConfigurableOption.screenWidth));
		Label heightL = new Label("HEIGHT");
		TextField heightF = new TextField(String.valueOf(ConfigurableOption.screenHeight));
		Button applyResolutionBtn = new Button("Apply");
		resolutionSetting.getChildren().addAll(widthL,widthF,heightL,heightF,applyResolutionBtn);
		applyResolutionBtn.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				try {
					ConfigurableOption.screenWidth = Integer.parseInt(widthF.getText());
					ConfigurableOption.screenHeight = Integer.parseInt(heightF.getText());
				} catch (NumberFormatException e) {
					// TODO: handle exception
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText(e.getMessage());
					alert.showAndWait();
					
					widthF.setText(String.valueOf(ConfigurableOption.screenWidth));
					heightF.setText(String.valueOf(ConfigurableOption.screenHeight));
				}
				Main.instance.resizeStage();
			}
		});
		GridPane otherSetting = new GridPane();
		otherSetting.setHgap(10);
		otherSetting.setVgap(40);
		otherSetting.setPadding(new Insets(10));
		otherSetting.setAlignment(Pos.TOP_CENTER);
		Label cmindL = new Label("Creation min delay");
		Spinner<Integer> cmindF = new Spinner<>(30,150,ConfigurableOption.objectCreationMinDelay,10);
		Label cmaxdL = new Label("Creation max delay");
		Spinner<Integer> cmaxdF = new Spinner<>(200,400,ConfigurableOption.objectCreationMaxDelay,10);
		Label mindL = new Label("Object min duration");
		Spinner<Integer> mindF = new Spinner<>(50,500,ConfigurableOption.objectMinDuration,10);
		Label maxdL = new Label("Object max duration");
		Spinner<Integer> maxdF =new Spinner<>(600,1000,ConfigurableOption.objectMaxDuration,10);
		Label tlL = new Label("Time limit (sec)");
		Spinner<Integer> tlF = new Spinner<>(30,300,ConfigurableOption.timelimit,10);
		otherSetting.add(cmindL, 0, 0);
		otherSetting.add(cmindF, 1, 0);
		otherSetting.add(cmaxdL, 2, 0);
		otherSetting.add(cmaxdF, 3, 0);
		otherSetting.add(mindL, 0, 1);
		otherSetting.add(mindF, 1, 1);
		otherSetting.add(maxdL, 2, 1);
		otherSetting.add(maxdF, 3, 1);
		otherSetting.add(tlL, 0, 2);
		otherSetting.add(tlF, 1, 2);
		optionPane.setTop(resolutionSetting);
		optionPane.setCenter(otherSetting);
		setCenter(optionPane);
		
		newGame.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				ConfigurableOption.objectCreationMaxDelay=cmaxdF.getValue();
				ConfigurableOption.objectCreationMinDelay=cmindF.getValue();
				ConfigurableOption.objectMaxDuration=maxdF.getValue();
				ConfigurableOption.objectMinDuration=mindF.getValue();
				ConfigurableOption.timelimit=tlF.getValue();
				Main.instance.toggleScene();
			}
		});
		
		viewScore.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				HighScoreUtility.displayTop10();
			}
		});
		
	}
	
	public void applyResize(){
		setPrefSize(ConfigurableOption.screenWidth, ConfigurableOption.screenHeight);
	}
}
