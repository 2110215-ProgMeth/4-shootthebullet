package ui;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import lib.*;

public class GameScreen extends StackPane{

	private IRenderableHolder renderableHolder;
	private Canvas canvas;
	
	public GameScreen(IRenderableHolder holder){
		super();
		this.canvas = new Canvas(ConfigurableOption.screenWidth, ConfigurableOption.screenHeight);
		renderableHolder=holder;
		getChildren().add(canvas);
		addListener();
	}
	
	public void paintComponenet(){
		GraphicsContext gc = this.canvas.getGraphicsContext2D();
		gc.setFill(Color.BLACK);
		gc.clearRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());
		gc.fillRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());
		for(IRenderableObject renderable : renderableHolder.getSortedRenderableObject()){
			renderable.render(gc);
		}
	}
	
	public void requestFocusForCanvas(){
		this.requestFocus();
	}
	
	private void addListener() {
		addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				if(event.getButton()==MouseButton.PRIMARY){
					InputUtility.setMouseLeftDown(true);
					InputUtility.setMouseLeftLastDown(true);
				}
			}
		});
		addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if(event.getButton()==MouseButton.PRIMARY){
					InputUtility.setMouseLeftDown(false);
				}
			}
		});
		addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				InputUtility.setMouseOnScreen(true);
			}
		});
		addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				InputUtility.setMouseOnScreen(false);
			}
		});
		addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				if(InputUtility.isMouseOnScreen()){
					InputUtility.setMouseX((int)event.getX());
					InputUtility.setMouseY((int)event.getY());
				}
			}
		});
		addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				if(InputUtility.isMouseOnScreen()){
					InputUtility.setMouseX((int)event.getX());
					InputUtility.setMouseY((int)event.getY());
				}
			}
		});
		
		addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				// TODO Auto-generated method stub
				InputUtility.setKeyPressed(event.getCode(), true);
				if(!InputUtility.getKeyTriggerFlag(event.getCode())){
					InputUtility.setKeyTriggered(event.getCode(), true);
				}
				InputUtility.setKeyTriggerFlag(event.getCode(), true);
			}
		});
		addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				// TODO Auto-generated method stub
				InputUtility.setKeyPressed(event.getCode(), false);
				InputUtility.setKeyTriggerFlag(event.getCode(), false);
			}
		});
	}
	
	public void applyResize(){
		setPrefSize(ConfigurableOption.screenWidth, ConfigurableOption.screenHeight);
		canvas.setHeight(ConfigurableOption.screenHeight);
		canvas.setWidth(ConfigurableOption.screenWidth);
	}
}
